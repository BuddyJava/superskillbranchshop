package com.skillbranch.bestshop.di.components;

import com.squareup.picasso.Picasso;

import dagger.Component;
import com.skillbranch.bestshop.di.modules.PicassoCacheModule;
import com.skillbranch.bestshop.di.scopes.RootScope;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
