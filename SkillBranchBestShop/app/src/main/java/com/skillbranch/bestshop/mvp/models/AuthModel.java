package com.skillbranch.bestshop.mvp.models;

public class AuthModel extends AbstractModel implements IAuthModel {

    public AuthModel() {
    }

    public boolean isAuthUser() {
        return mDataManager.getPreferencesManager().getAuthToken() != null;
    }

    public void saveAuthToken(String authToken) {
        mDataManager.getPreferencesManager().saveAuthToken(authToken);
    }

}
