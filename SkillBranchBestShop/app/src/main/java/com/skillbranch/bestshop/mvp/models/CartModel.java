package com.skillbranch.bestshop.mvp.models;

import com.skillbranch.bestshop.data.storage.realm.OrdersRealm;

import io.realm.RealmResults;

public class CartModel extends AbstractModel {
    public RealmResults<OrdersRealm> getAllOrders() {
        return mDataManager.getAllOrders();
    }
}
