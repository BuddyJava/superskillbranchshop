package com.skillbranch.bestshop.mvp.views;


import android.support.annotation.Nullable;
import android.view.View;

import com.skillbranch.bestshop.data.storage.dto.UserInfoDto;
import com.skillbranch.bestshop.mvp.presenters.AbstractPresenter;

public interface IRootView extends IView {
    void showMessage (String message);
    void showError (Throwable e);

    void showLoad();
    void hideLoad();

    void showBasketCounter();

    @Nullable
    IView getCurrentScreen();

//    void pickAvatarFromGallery();
//    void pickAvatarFromCamera();
//    void setAvatarListener(RootActivity.AvatarListener avatarListener);
    void updateCartProductCounter();
    void initDrawer(UserInfoDto userInfoDto);


    void setMenuItemChecked(AbstractPresenter vmAbstractPresenter);
}
