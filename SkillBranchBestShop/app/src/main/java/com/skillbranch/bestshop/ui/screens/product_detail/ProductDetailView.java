package com.skillbranch.bestshop.ui.screens.product_detail;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;

import javax.inject.Inject;

import butterknife.BindView;
import flow.Flow;

import com.skillbranch.bestshop.R;
import com.skillbranch.bestshop.data.storage.realm.CommentRealm;
import com.skillbranch.bestshop.data.storage.realm.ProductRealm;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.mvp.views.AbstractView;
import com.skillbranch.bestshop.mvp.views.IProductDetailView;

public class ProductDetailView extends AbstractView<ProductDetailScreen.ProductDetailPresenter> implements IProductDetailView {

    @BindView(R.id.more_info_pager)
    ViewPager mViewPager;
    @BindView(R.id.more_info_tabs)
    TabLayout mTabLayout;
    @Inject
    ProductDetailScreen.ProductDetailPresenter mPresenter;

    public ProductDetailView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void initDagger(Context context) {
        DaggerService.<ProductDetailScreen.Component>getDaggerComponent(context).inject(this);
    }

    //region ============================== IProductDetailView ==============================

    @Override
    public void initView(ProductRealm productRealm) {
        setupViewPager(productRealm);
    }

    private void setupViewPager(ProductRealm productRealm) {
        mViewPager.setAdapter(new DetailAdapter(getContext(), productRealm));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mPresenter.switchFab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTabLayout.setupWithViewPager(mViewPager);
        mPresenter.switchFab(mViewPager.getCurrentItem());
    }


    @Override
    public boolean viewOnBackPressed() {
        return false;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    //endregion

    //region ============= Events ===============


    public void addComment() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_comment_add, null);
        RatingBar addCommentRatingBar = (RatingBar) view.findViewById(R.id.add_comment_ratingbar);
        TextInputEditText addCommentEditText = (TextInputEditText) view.findViewById(R.id.comment_message);

        dialogBuilder.setTitle(R.string.account_editing)
                .setView(view)
                .setPositiveButton("Ok", (dialogInterface, i) -> {

                    CommentRealm comment = new CommentRealm(addCommentRatingBar.getRating(), addCommentEditText.getText().toString());
                    mPresenter.addComment(comment);
//                    mPresenter.sendToServer(addCommentRatingBar.getRating(), addCommentEditText.getText().toString());

                    dialogInterface.cancel();
                })
                .setNegativeButton("Nope", (dialogInterface, i) -> dialogInterface.cancel())
                .show();
    }
    //endregion
}
