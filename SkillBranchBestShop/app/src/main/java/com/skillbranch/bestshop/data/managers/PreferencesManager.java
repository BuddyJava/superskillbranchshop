package com.skillbranch.bestshop.data.managers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.skillbranch.bestshop.data.storage.dto.ProductDto;
import com.skillbranch.bestshop.utils.ConstantManager;
import com.skillbranch.bestshop.utils.MvpAuthApplication;

public class PreferencesManager {
    public static final String PROFILE_FULL_NAME_KEY = "profileFullNameKey";
    public static final String PROFILE_AVATAR_KEY = "profileAvatarKey";
    public static final String PROFILE_PHONE_KEY = "profilePhoneKey";
    public static final String NOTIFICATION_ORDER_KEY = "notificationOrderKey";
    public static final String NOTIFICATION_PROMO_KEY = "notificationPromoKey";
    private static final String PRODUCT_LAST_UPDATE_KEY = "PRODUCT_LAST_UPDATE_KEY";
    private static final String MOCK_PRODUCT_LIST = "MOCK_PRODUCT_LIST";

    private SharedPreferences mSharedPreferences;
    Context mContext;
//    private Moshi mMoshi;
//    private JsonAdapter<UserAddressDto> mJsonAdapter;
    private List<ProductDto> mProductDtoList = new ArrayList<>();

    public PreferencesManager(Context context) {
        this.mSharedPreferences= MvpAuthApplication.getSharedPreferences();
        mContext = context;

        initProductsMockData();

//        mMoshi = new Moshi.newFabBuilder().build();
//        mJsonAdapter = mMoshi.adapter(UserAddressDto.class);
    }

    public String getLastProductUpdate() {
        return mSharedPreferences.getString(PRODUCT_LAST_UPDATE_KEY, "Thu, 01 Jan 1970 00:00:00 GMT");
    }

    public void saveLastProductUpdate(String lastModified) {
//        Log.e(TAG, "saveLastProductUpdate: "+lastModified);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PRODUCT_LAST_UPDATE_KEY, lastModified);
        editor.apply();
    }

    public void saveAuthToken (String authToken){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN, authToken);
        editor.apply();
    }

    public void saveBasketCounter(int count) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(ConstantManager.BASKET_COUNT, count);
        editor.apply();
    }

    public int getBasketCounter() {
        return mSharedPreferences.getInt(ConstantManager.BASKET_COUNT, 0);
    }

    public String getAuthToken (){
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, null);
    }

    public boolean isUserAuth() {
        return !"".equals(mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, ""));
    }

    void saveProfileInfo(String name, String phone, String avatarUri) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PROFILE_FULL_NAME_KEY, name);
        editor.putString(PROFILE_PHONE_KEY, phone);
        editor.putString(PROFILE_AVATAR_KEY, avatarUri);
        editor.apply();
    }

    Map<String, String> loadProfileInfo() {
        Map<String, String> profileInfo = new HashMap<>();
        if (mSharedPreferences.contains(PROFILE_FULL_NAME_KEY)) {
            profileInfo.put(PROFILE_FULL_NAME_KEY, mSharedPreferences.getString(PROFILE_FULL_NAME_KEY, "null"));
        }
        if (mSharedPreferences.contains(PROFILE_PHONE_KEY)) {
            profileInfo.put(PROFILE_PHONE_KEY, mSharedPreferences.getString(PROFILE_PHONE_KEY, "null"));
        }
        if (mSharedPreferences.contains(PROFILE_AVATAR_KEY)) {
            profileInfo.put(PROFILE_AVATAR_KEY, mSharedPreferences.getString(PROFILE_AVATAR_KEY, "null"));
        }
        return profileInfo;
    }

    void saveSettings(String notificationKey, boolean isChecked) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(notificationKey, isChecked);
        editor.apply();
    }

    Map<String, Boolean> loadSettings() {
        Map<String, Boolean> settings = new HashMap<>();
        if (mSharedPreferences.contains(NOTIFICATION_ORDER_KEY)) {
            settings.put(NOTIFICATION_ORDER_KEY, mSharedPreferences.getBoolean(NOTIFICATION_ORDER_KEY, false));
        }
        if (mSharedPreferences.contains(NOTIFICATION_PROMO_KEY)) {
            settings.put(NOTIFICATION_PROMO_KEY, mSharedPreferences.getBoolean(NOTIFICATION_PROMO_KEY, false));
        }
        return settings;
    }

    public void generateProductsMockData(List<ProductDto> mockProductList) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String products = gson.toJson(mockProductList);
        if (mSharedPreferences.getString(MOCK_PRODUCT_LIST, null) == null) {
            editor.putString(MOCK_PRODUCT_LIST, products);
            editor.commit();
        }
    }

    private void initProductsMockData() {
        String products = mSharedPreferences.getString(MOCK_PRODUCT_LIST, null);
        if (products != null) {
            Gson gson = new Gson();
            ProductDto[] productList = gson.fromJson(products, ProductDto[].class);
            List<ProductDto> productDtoList = Arrays.asList(productList);
            mProductDtoList = new ArrayList<>(productDtoList);
        }
    }

    public List<ProductDto> getProductList() {
        return mProductDtoList;
    }

    public void saveUserAvatar(String avatarUrl) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PROFILE_AVATAR_KEY, avatarUrl);
        editor.apply();
    }

    public String getUserAvatar() {
        return mSharedPreferences.getString(PROFILE_AVATAR_KEY, "http://skill-branch.ru/img/app/avatar-1.png");
    }

    public String getUserName() {
        return mSharedPreferences.getString(PROFILE_FULL_NAME_KEY, "Unknown");
    }
}
