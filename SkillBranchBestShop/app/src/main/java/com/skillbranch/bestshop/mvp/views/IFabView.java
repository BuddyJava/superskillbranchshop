package com.skillbranch.bestshop.mvp.views;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

public interface IFabView {
    void setVisibleFab(boolean isVisible);
    void setImageResFab(int imageRes);
    void setOnClickListenerFab(View.OnClickListener listener);
    void setBehaviorFab(FloatingActionButton.Behavior behavior);
}
